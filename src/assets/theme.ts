import { createMuiTheme } from "@material-ui/core";

export const theme = createMuiTheme({
  spacing: 8,
});
