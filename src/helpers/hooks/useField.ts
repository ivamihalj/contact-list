import { ChangeEvent, useCallback, useState } from "react";

export interface FieldHookReturnValue {
  value: string;
  onChange(e: ChangeEvent<HTMLInputElement>): void;
}

/** Used to handle a form field change.
 * @param {string=} [initValue=""] Initial value of the field.
 */
export const useField = (initValue = ""): FieldHookReturnValue => {
  const [value, setValue] = useState(initValue);
  const onChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => setValue(e.target.value),
    [],
  );
  return { value, onChange };
};
