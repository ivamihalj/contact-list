import { useMemo } from "react";
import { useDispatch } from "react-redux";
import { ActionCreator, bindActionCreators } from "redux";

/** Used to create a dispatch of a redux action.
 * @param {*} action Action to dispatch.
 */
const useCreateAction: any = <Action> (
  action: ActionCreator<Action>,
) => {
  const dispatch = useDispatch();
  return useMemo(() => bindActionCreators(action, dispatch), [dispatch, action]);
};

export default useCreateAction;
