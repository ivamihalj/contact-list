import { Box, Grid } from "@material-ui/core";
import { AddOutlined, Close, CloseOutlined, Delete, KeyboardReturn, MailOutline, PersonOutline, PhoneOutlined, PublishOutlined } from "@material-ui/icons";
import React from "react";
import { FormProps } from "./types";
import "./styles.scss";
import { FormattedMessage } from "react-intl";
import InputContainer from "./../shared/input/container";
import ButtonContainer from "../shared/button/container";
import ConfirmDialogContainer from "../shared/confirmDialog/container";

/** Renders the Add/Edit contact form.
 * @param {() => void} addNewNumber Adds a new number to the list.
 * @param {() => void} deleteContact Deletes the contact (if in edit mode).
 * @param {() => void} editNumberProperty Edits a contact number label or value
 * @param {() => void} goBack Navigates to the previous route.
 * @param {() => void} handleImageUpload Handles the upload of the contact picture.
 * @param {(e: React.FormEvent) => void} handleSubmit Handles form submission.
 * @param {(index: number) => void} removeNumber Removes a number from the contact numbers list.
 * @param {() => void} resetFormValidation Sets validation for every field to true.
 * @param {() => void} resetImage Removes the current image.
 * @param {() => void} toggleDeleteDialog Toggles Confirm Delete dialog open/closed.
 * @param {FieldHookReturnValue} emailField Contains e-mail input.
 * @param {MutableRefObject<any>} imageElement Ref for the HTML element that display the image.
 * @param {FieldHookReturnValue} imageField Contains contact image input.
 * @param {boolean} isDeleteDialogOpen Marks if the Confirm Delete dialog is opne.
 * @param {boolean} isEdit Marks if the open form is in the EDIT or NEW mode.
 * @param {boolean} isEmailValid Marks if the current e-mail input is a valid e-mail address.
 * @param {boolean} isNameValid Marks if the current name input is a valid name.
 * @param {FieldHookReturnValue} nameField Contains Full Name input.
 * @param {ContactNumberInput[]} numberInputs Contains contact numbers.
*/
const FormPresenter: React.FunctionComponent<FormProps> = ({
  addNewNumber,
  deleteContact,
  editNumberProperty,
  goBack,
  handleImageUpload,
  handleSubmit,
  removeNumber,
  resetFormValidation,
  resetImage,
  toggleDeleteDialog,
  emailField,
  imageElement,
  imageField,
  isDeleteDialogOpen,
  isEdit,
  isEmailValid,
  isNameValid,
  nameField,
  numberInputs,
}) => (
  <Grid
    className="form"
    container
    justify="center"
    spacing={3}
  >
    {/* Delete dialog */}
    <ConfirmDialogContainer
      isOpen={isDeleteDialogOpen}
      label="common.delete"
      message="common.confirm.delete"
      confirmLabel="common.delete"
      cancelLabel="common.cancel"
      onConfirm={deleteContact}
      onCancel={toggleDeleteDialog}
    />
    {/* Contact image */}
    <input
      type="file"
      ref={imageElement}
      onChange={handleImageUpload}
      style={{ display: "none" }}
      accept="image/x-png,image/gif,image/jpeg"
    />
    <Grid item xs={12} md={3} lg={2} xl={1} className="form__image-container">
      <Grid
        className="form__image"
        style={{
          backgroundImage: `url(${imageField})`,
        }}
        onClick={() => { !imageField || imageField === "" ? imageElement.current.click() : resetImage(); }}
      >
        { !imageField || imageField === "" ? <PublishOutlined fontSize="large" /> : <Close fontSize="large" /> }
      </Grid>
    </Grid>
    {/* Contact info */}
    <Grid item md={6}>
      {/* Header: back and delete buttons */}
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        className="form__header"
        pb={2}
        mb={4}
      >
        {/* Back button */}
        <KeyboardReturn
          className="form__button --back"
          onClick={goBack}
        />
        {/* Delete button */}
        {isEdit && (
          <Box
            display="inline-flex"
            alignItems="center"
            className="form__button"
            onClick={toggleDeleteDialog}
          >
            <Box mr={2}>
              <FormattedMessage id="common.delete" />
            </Box>
            <Delete fontSize="small" className="form__button"/>
          </Box>
        )}
      </Box>
      {/* Form */}
      <form onSubmit={handleSubmit}>
        <Box mb={8} className="form__main">
          {/* Name field */}
          <Box className="form__main__field" mb={2} pb={4}>
            {/* Name label */}
            <Box className="form__main__field__label" mb={2}>
              <Box mr={1} display="flex" alignItems="center">
                <PersonOutline fontSize="small" />
              </Box>
              <FormattedMessage id="form.name" />
            </Box>
            {/* Name input */}
            <Box className="form__main__input">
              <InputContainer
                isInvalid={!isNameValid}
                placeholder="form.name"
                name="name"
                onFocus={resetFormValidation}
                {...nameField}
              />
            </Box>
          </Box>
          {/* Email field */}
          <Box className="form__main__field" mb={2} pb={4}>
            {/* Email label */}
            <Box className="form__main__field__label" mb={2}>
              <Box mr={1} display="flex" alignItems="center">
                <MailOutline fontSize="small" />
              </Box>
              <FormattedMessage id="contact.email" />
            </Box>
            {/* Email input */}
            <Box className="form__main__input">
              <InputContainer
                isInvalid={!isEmailValid}
                type="email"
                placeholder="contact.email"
                name="email"
                onFocus={resetFormValidation}
                {...emailField}
              />
            </Box>
          </Box>
          {/* Number field */}
          <Box className="form__main__field" mb={2} pb={4}>
            {/* Number label */}
            <Box className="form__main__field__label" mb={2}>
              <Box mr={1} display="flex" alignItems="center">
                <PhoneOutlined fontSize="small" />
              </Box>
              <FormattedMessage id="contact.numbers" />
            </Box>
            {/* Number inputs */}
            {numberInputs.map((number, index) => (
            // Row
              <Box mb={4} key={`key.${index}`}>
                <Grid container spacing={2} alignItems="center">
                  {/* Number value */}
                  <Grid item xs={12} md={6}>
                    <InputContainer
                      placeholder="form.number"
                      value={number.value}
                      onChange={(e) => editNumberProperty("value", e, index)}
                      isInvalid={!number.isNumberValid}
                      onFocus={resetFormValidation}
                    />
                  </Grid>
                  {/* Number key */}
                  <Grid item xs={10} md={5}>
                    <InputContainer
                      placeholder="form.label"
                      value={number.key}
                      onChange={(e) => editNumberProperty("key", e, index)}
                      isInvalid={!number.isKeyValid}
                      onFocus={resetFormValidation}
                    />
                  </Grid>
                  {/* Delete number button */}
                  <Grid
                    item
                    xs={2} md={1}
                    onClick={() => removeNumber(index)}
                    className="form__main__field__button-container"
                  >
                    <Box display="inline-flex" alignItems="center" className="form__main__field__button">
                      <CloseOutlined fontSize="small" />
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            ))}
            {/* Add number box */}
            <Box
              display="inline-flex"
              alignItems="center"
              className="form__main__field__label --add"
              onClick={addNewNumber}
            >
              <Box mr={1} display="inline-flex" alignItems="center" className="form__main__field__button --add">
                <AddOutlined fontSize="small" />
              </Box>
              <FormattedMessage id="form.add.number" />
            </Box>
          </Box>
        </Box>
        {/* Buttons */}
        <Grid container justify="space-between">
          <Grid item className="form__footer-button">
            <ButtonContainer label="common.cancel" type="button" onClick={goBack} variant="secondary" />
          </Grid>
          <Grid item className="form__footer-button">
            <ButtonContainer label="form.save" type="submit" variant="primary" />
          </Grid>
        </Grid>
      </form>
    </Grid>
  </Grid>
);

export default FormPresenter;
