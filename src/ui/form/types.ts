import { ChangeEvent, MutableRefObject } from "react";
import { FieldHookReturnValue } from "./../../helpers/hooks/useField";
import { ContactNumberInput } from "./../contacts/types";

export interface FormProps {
  goBack(): void;
  handleSubmit(values: any): void;
  handleImageUpload(): void;
  addNewNumber(): void;
  removeNumber(index: number): void;
  resetImage(): void;
  resetFormValidation(): void;
  deleteContact(): void;
  toggleDeleteDialog(): void;
  editNumberProperty(propetry: "key" | "value", e: ChangeEvent<HTMLInputElement>, index: number): void;
  isEdit: boolean;
  numberInputs: ContactNumberInput[];
  nameField: FieldHookReturnValue;
  emailField: FieldHookReturnValue;
  imageField?: string;
  imageElement: MutableRefObject<any>;
  isDeleteDialogOpen: boolean;
  isNameValid: boolean;
  isEmailValid: boolean;
}
