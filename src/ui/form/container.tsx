import React, { ChangeEvent, useRef, useState } from "react";
import FormPresenter from "./presenter";
import { Redirect, RouteComponentProps, useHistory, useLocation } from "react-router-dom";
import { HOME_URL, NEW_URL } from "./../../data/constants/urls";
import { useField } from "./../../helpers/hooks/useField";
import { useSelector } from "react-redux";
import { contactSelectors } from "../../state/selectors";
import { ContactNumberInput } from "./../contacts/types";
import useCreateAction from "../../helpers/hooks/useCreateAction";
import { ContactActions } from "../../state/actions";

/** Renders the Add/Edit contact form. */
const FormContainer: React.FunctionComponent<RouteComponentProps<{id: string}>> = ({
  match,
}) => {
  // MARK: @config
  /** Ref for the HTML element that displays the image. */
  const imageElement = useRef();
  /** Router history. */
  const history = useHistory();
  /** Currently active URL. */
  const currentRoute = useLocation().pathname;
  /** Marks if the open form is in the EDIT or NEW mode. */
  const isEdit = currentRoute !== NEW_URL;
  /** Contact ID (from URL). */
  const { id: contactId } = match.params;
  /** Currently open contact. */
  const contact = useSelector((state) => contactSelectors.getContactById(state, Number(contactId)));

  // MARK: @actions
  /** Handles contact creation. */
  const createContactAction = useCreateAction(ContactActions.createContact);
  /** Haandles contact edit. */
  const editContactAction = useCreateAction(ContactActions.editContact);
  /** Handles contact delete. */
  const deleteContactAction = useCreateAction(ContactActions.deleteContact);

  // MARK: @state
  /** Marks if the Confirm Delete dialog is open. */
  const [isDeleteDialogOpen, setDeleteDialog] = useState(false);
  /** Contains Full Name input. */
  const nameField = useField(contact?.name);
  /** Contains e-mail input. */
  const emailField = useField(contact?.email);
  /** Contains contact image input. */
  const [imageField, setImageField] = useState(contact?.image);
  /** Contains contact numbers. */
  const [numberInputs, changeNumberInputs] = useState<ContactNumberInput[]>(
    contact?.numbers?.map((number) => ({
      ...number,
      isKeyValid: true,
      isNumberValid: true,
    }))
    || [{
      key: "",
      value: "",
      isKeyValid: true,
      isNumberValid: true,
    }],
  );
  /** Marks if name input passes validation. */
  const [isNameValid, setIsNameValid] = useState(true);
  /** Marks if e-mail input passes validation. */
  const [isEmailValid, setIsEmailValid] = useState(true);

  // MARK: @helpers
  /** Reads the file from the upload.
   * @param imageEl HTML image element.
   */
  const readImage = (imageEl: any) => {
    if (imageEl?.files?.length > 0) {
      const reader = new FileReader();
      reader.onload = (e) => {
        setImageField(e.target?.result as string);
      };
      reader.readAsDataURL(imageEl.files[0]);
    }
  };

  // MARK: @methods
  /** Adds a new number to the list. */
  const addNewNumber = () => {
    const newInputs = numberInputs.concat({ key: "", value: "", isKeyValid: true, isNumberValid: true });
    changeNumberInputs(newInputs);
  };

  /** Deletes the contact (if in edit mode). */
  const deleteContact = () => {
    deleteContactAction(contact?.id);
    history.push(HOME_URL);
  };

  /** Edits a contact number label or value.
   * @param {"key" | "value"} property Marks if the changed property is a label or a number.
   * @param {ChangeEvent<HTMLInputElement>} e Input event.
   * @param {number} index Ordinal number of the number.
   */
  const editNumberProperty = (
    property: "key" | "value",
    e: ChangeEvent<HTMLInputElement>,
    index: number,
  ) => {
    e.preventDefault();
    const newInputs = [...numberInputs];
    newInputs[index] = {
      ...newInputs[index],
      [property]: e.target.value,
    };
    changeNumberInputs(newInputs);
  };

  /** Navigates to the previous route. */
  const goBack = () => history.goBack();

  /** Handles the upload of the contact picture. */
  const handleImageUpload = () => {
    const image = imageElement.current;
    readImage(image);
  };

  /** Handles form submission.
   * @param {React.FormEvent} e: Form submit event.
   */
  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    // Validation
    const nameRegex = new RegExp(/^((\S)+( )?)+$/i);
    const emailRegex = new RegExp(/^(\S){2,}@(\S){2,}\.(\d|\w){2,}$/i);
    const keyRegex = new RegExp(/^.+$/i);
    const valueRegex = new RegExp(/^(\+(\d){2,3}( )?)?((\d)+( )?)+$/im);

    const nameValid = nameField.value !== "" && nameRegex.test(nameField.value);
    const emailValid = emailField.value !== "" && emailRegex.test(emailField.value);

    setIsNameValid(nameValid);
    setIsEmailValid(emailValid);

    let numbersValid = true;
    const newInputs = [...numberInputs].map((input) => {
      const isKeyValid = input.key !== "" && keyRegex.test(input.key);
      const isNumberValid = input.value !== "" && valueRegex.test(input.value);
      if (!isKeyValid || !isNumberValid) { numbersValid = false; }
      return ({
        ...input,
        isKeyValid,
        isNumberValid,
      });
    });
    changeNumberInputs(newInputs);

    if (!nameValid || !emailValid || !numbersValid) { return; }

    // Submission
    const action = isEdit ? editContactAction : createContactAction;
    const contactBody = {
      id: contact?.id,
      isFavorite: Boolean(contact?.isFavorite),
      name: nameField.value,
      image: imageField,
      email: emailField.value,
      numbers: numberInputs,
    };
    action(contactBody);
    history.push(HOME_URL);
  };

  /** Removes a number from the contact numbers list.
   * @param {number} index Ordinal number of the number to delete.
   */
  const removeNumber = (index: number) => {
    const newInputs = [...numberInputs];
    newInputs.splice(index, 1);
    changeNumberInputs(newInputs);
  };

  /** Sets every field as valid. */
  const resetFormValidation = () => {
    setIsNameValid(true);
    setIsEmailValid(true);
    const newInputs = [...numberInputs].map((input) => ({
      ...input,
      isKeyValid: true,
      isNumberValid: true,
    }));
    changeNumberInputs(newInputs);
  };

  /** Removes the image from the form. */
  const resetImage = () => {
    setImageField("");
  };

  /** Toggles Confirm Delete dialog open/closed. */
  const toggleDeleteDialog = () => {
    setDeleteDialog(!isDeleteDialogOpen);
  };

  // MARK: @render
  if (isEdit && !contact) {
    return <Redirect to={HOME_URL} />;
  }

  return (
    <FormPresenter
      {...{
        goBack,
        isEdit,
        handleImageUpload,
        handleSubmit,
        addNewNumber,
        removeNumber,
        nameField,
        emailField,
        imageField,
        imageElement,
        numberInputs,
        editNumberProperty,
        isDeleteDialogOpen,
        toggleDeleteDialog,
        deleteContact,
        isNameValid,
        isEmailValid,
        resetFormValidation,
        resetImage,
      }}
    />
  );
};

export default FormContainer;
