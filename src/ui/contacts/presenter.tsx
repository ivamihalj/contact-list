import { Box, Grid } from "@material-ui/core";
import { Search } from "@material-ui/icons";
import React from "react";
import AddNewContainer from "./addNew/container";
import ContactContainer from "./contact/container";
import NavigationContainer from "./navigation/container";
import "./styles.scss";
import { ContactPresenterProps } from "./types";

/** Renders the list of contacts.
 * @param {(e: ChangeEvent<HTMLInputElement>) => void} filterContacts Filters contacts by name.
 * @param {Contact[]} contacts List of contacts to show.
 * @param {boolean} isFavorites Is current route for the Favorite contacts.
*/
const ContactsPresenter: React.FunctionComponent<ContactPresenterProps> = ({
  filterContacts,
  contacts,
  isFavorites,
}) => (
  <Box className="contacts">
    {/* Toggle between all and favorite contacts */}
    <NavigationContainer />
    {/* Search box */}
    <Box className="contacts__search-container">
      <Box className="contacts__search">
        <Search className="contacts__search__icon" />
        <input type="text" onChange={filterContacts} />
      </Box>
    </Box>
    <Box className="contacts__main">
      <Grid container spacing={4}>
        {/* Add new contact button */}
        {!isFavorites && (
          <Grid item xs={12} md={3} className="contacts__item">
            <AddNewContainer />
          </Grid>
        )}
        {/* Contacts grid */}
        {contacts.map((contact) => (
          <Grid item xs={12} md={3} key={contact.id} className="contacts__item">
            <ContactContainer {...{ contact }}/>
          </Grid>
        ))}
      </Grid>
    </Box>
  </Box>
);

export default ContactsPresenter;
