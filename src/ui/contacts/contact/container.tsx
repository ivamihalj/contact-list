import React, { useState } from "react";
import { ContactActions } from "../../../state/actions";
import ContactPresenter from "./presenter";
import { ContactProps } from "./types";
import useCreateAction from "./../../../helpers/hooks/useCreateAction";
import { useHistory } from "react-router-dom";

/** Renders a single contact card.
 * @param {Contact} contact Contact to display the card for.
 */
const ContactContainer: React.FunctionComponent<ContactProps> = ({
  contact,
}) => {
  // MARK: @state
  /** Marks if Confirm Delete dialog is open. */
  const [isDeleteDialogOpen, setDeleteDialog] = useState(false);

  // MARK: @config
  /** Router history. */
  const history = useHistory();

  // MARK: @actions
  /** Handles contact delete. */
  const deleteContactAction = useCreateAction(ContactActions.deleteContact);
  /** Handles adding to or removing from favorite contacts. */
  const toggleFavoriteAction = useCreateAction(ContactActions.toggleFavoriteContact);

  // MARK: @methods
  /** Deletes a contact from the store. */
  const deleteContact = () => deleteContactAction(contact.id);

  /** Opens the edit page.
   * @param {React.MouseEvent<SVGSVGElement>} e Click event.
   */
  const editContact = (e: React.MouseEvent<SVGSVGElement>) => {
    e.stopPropagation();
    history.push(`/edit/${contact.id}`);
  };

  /** Opens contact details. */
  const openDetails = () => {
    history.push(`/details/${contact.id}`);
  };

  /** Toggles confirm delete dialog open/close.
   * @param {React.MouseEvent<SVGSVGElement>} e Click event.
   */
  const toggleDeleteDialog = (e: React.MouseEvent<SVGSVGElement>) => {
    e.stopPropagation();
    setDeleteDialog(!isDeleteDialogOpen);
  };

  /** Toggles a contact favorite.
   * @param {React.MouseEvent<SVGSVGElement>} e Click event.
   */
  const toggleFavorite = (e: React.MouseEvent<SVGSVGElement>) => {
    e.stopPropagation();
    toggleFavoriteAction(contact.id);
  };

  // MARK: @render
  return (
    <ContactPresenter
      {...{
        deleteContact,
        editContact,
        openDetails,
        toggleDeleteDialog,
        toggleFavorite,
        contact,
        isDeleteDialogOpen,
      }}
    />
  );
};

export default ContactContainer;
