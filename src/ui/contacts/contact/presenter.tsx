import { Box, Grid } from "@material-ui/core";
import { Delete, Edit, Favorite, FavoriteBorder } from "@material-ui/icons";
import React from "react";
import ConfirmDialogContainer from "../../shared/confirmDialog/container";
import "./styles.scss";
import { ContactPresenterProps } from "./types";

/** Renders a single contact card.
 * @param {(e: React.MouseEvent<SVGSVGElement>) => void} deleteContact Deletes a contact from the store.
 * @param {(e: React.MouseEvent<SVGSVGElement>) => void} editContact Opens the edit page.
 * @param {() => void} openDetails Opens contact details.
 * @param {(e: React.MouseEvent<SVGSVGElement>) => void} toggleFavorite Toggles a contact favorite.
 * @param {Contact} contact Contact to display the card for.
 * @param {boolean} isDeleteDialogOpen Marks if Confirm Delete dialog is open.
 */
const ContactPresenter: React.FunctionComponent<ContactPresenterProps> = ({
  deleteContact,
  editContact,
  openDetails,
  toggleFavorite,
  toggleDeleteDialog,
  contact,
  isDeleteDialogOpen,
}) => (
  <Grid
    container
    className="contact"
    onClick={openDetails}
  >
    {/* Confirm Delete dilog */}
    <ConfirmDialogContainer
      isOpen={isDeleteDialogOpen}
      label="common.delete"
      message="common.confirm.delete"
      confirmLabel="common.delete"
      cancelLabel="common.cancel"
      onConfirm={deleteContact}
      onCancel={toggleDeleteDialog}
    />
    {/* Contact image */}
    <Grid item xs={2} md={12} className="contact__image">
      { contact.image ? <img alt="Contact" src={contact.image} /> : <Box /> }
    </Grid>
    {/* Contact name */}
    <Grid item className="contact__name" xs={6} md={12}>
      {contact.name}
    </Grid>
    {/* Contact icons */}
    <Grid item className="contact__icons" xs={4} md={12}>
      <Grid container justify="space-between">
        {/* Favorite contact icon */}
        <Grid item className="contact__icons__favorite" xs={4} md={9}>
          {contact.isFavorite
            ? <Favorite fontSize="small" className="contact__icons__icon" onClick={toggleFavorite} />
            : <FavoriteBorder fontSize="small" className="contact__icons__icon" onClick={toggleFavorite} />}
        </Grid>
        {/* Edit contact icon */}
        <Grid item className="contact__icons__manage" xs={4} md={2}>
          <Edit fontSize="small" className="contact__icons__icon --edit" onClick={editContact} />
        </Grid>
        {/* Delete contact icon */}
        <Grid item xs={4} md={1}>
          <Delete fontSize="small" className="contact__icons__icon" onClick={toggleDeleteDialog} />
        </Grid>
      </Grid>
    </Grid>
  </Grid>
);

export default ContactPresenter;
