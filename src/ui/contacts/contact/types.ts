import { Contact } from "../types";

export interface ContactProps {
  contact: Contact;
}

export interface ContactPresenterProps extends ContactProps {
  toggleFavorite(e: React.MouseEvent<SVGSVGElement>): void;
  toggleDeleteDialog(e: React.MouseEvent<SVGSVGElement>): void;
  editContact(e: React.MouseEvent<SVGSVGElement>): void;
  deleteContact(): void;
  openDetails(): void;
  isDeleteDialogOpen: boolean;
}
