import React, { ChangeEvent, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { FAVORITES_URL } from "../../data/constants/urls";
import { contactSelectors } from "../../state/selectors";
import ContactsPresenter from "./presenter";
import { Contact } from "./types";

/** Renders the list of contacts. */
const ContactsContainer: React.FunctionComponent = () => {
  // MARK: @config
  /** Current router location. */
  const currentRoute = useLocation().pathname;
  /** Is current route for the Favorite contacts. */
  const isFavorites = currentRoute === FAVORITES_URL;
  /** List of contacts (favorite or all, based on the route). */
  const contacts = useSelector(isFavorites
    ? contactSelectors.getFavoriteContacts
    : contactSelectors.getAllContacts,
  );

  // MARK: @state
  const [contactsToDisplay, setContactsToDisplay] = useState<Contact[]>(contacts);

  // MARK: @effects
  useEffect(() => {
    setContactsToDisplay(contacts);
  }, [contacts]);

  // MARK: @methods
  /** Filters contacts by name.
   * If search term includes one word only, it returns an item if first name OR last name include the search term.
   * If search term includes multiple words, it searches by full name.
   * @param e Input change event.
   */
  const filterContacts = (e: ChangeEvent<HTMLInputElement>) => {
    const searchTerm = e.target.value.toUpperCase();
    const filteredContacts = contacts.filter((contact) => {
      const nameParts = contact.name.split(" ");
      return nameParts.some((part) => part.toUpperCase().startsWith(searchTerm))
        || (searchTerm.split(" ").length > 1 && contact.name.toUpperCase().includes(searchTerm));
    });
    setContactsToDisplay(filteredContacts);
  };

  // MARK: @render
  return (
    <ContactsPresenter
      {...{ filterContacts, isFavorites }}
      contacts={contactsToDisplay}
    />
  );
};

export default ContactsContainer;
