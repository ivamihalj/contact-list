import React from "react";
import { NavLink } from "react-router-dom";
import "./styles.scss";
import { FAVORITES_URL, HOME_URL } from "./../../../data/constants/urls";
import { FormattedMessage } from "react-intl";
import { Grid } from "@material-ui/core";

/** Renders the All/Favorites navigation. */
const NavigationPresenter: React.FunctionComponent = () => (
  <Grid container xs={12} className="navigation" spacing={8}>
    {/* All contacts */}
    <Grid item xs={6} className="navigation__item">
      <NavLink
        exact
        to={HOME_URL}
        className="navigation__link"
        activeClassName="active"
      >
        <FormattedMessage id="contacts.all" />
      </NavLink>
    </Grid>
    {/* Favorite contacts */}
    <Grid item xs={6} className="navigation__item">
      <NavLink
        exact
        to={FAVORITES_URL}
        className="navigation__link"
        activeClassName="active"
      >
        <FormattedMessage id="contacts.favorites" />
      </NavLink>
    </Grid>
  </Grid>
);

export default NavigationPresenter;
