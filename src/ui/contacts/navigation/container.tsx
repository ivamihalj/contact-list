import React from "react";
import NavigationPresenter from "./presenter";

/** Renders the All/Favorites navigation. */
const NavigationContainer: React.FunctionComponent = () => (
  // MARK: @render
  <NavigationPresenter />
);

export default NavigationContainer;
