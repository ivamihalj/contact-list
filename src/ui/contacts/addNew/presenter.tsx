import React from "react";
import "./styles.scss";
import { Add } from "@material-ui/icons";
import { FormattedMessage } from "react-intl";
import { AddNewProps } from "./types";
import { Grid } from "@material-ui/core";

/** Renders the Add New Contact button.
 * @param {() => void} onAddButtonClick Handles click on the Add New button.
  */
const AddNewPresenter: React.FunctionComponent<AddNewProps> = ({
  onAddButtonClick,
}) => (
  <Grid container className="add-new" onClick={onAddButtonClick}>
    {/* Icon */}
    <Grid item xs={2} md={12} className="add-new__icon">
      <Add fontSize="large"/>
    </Grid>
    {/* Text */}
    <Grid item xs={10} md={12} className="add-new__text">
      <FormattedMessage id="contact.add" />
    </Grid>
  </Grid>
);

export default AddNewPresenter;
