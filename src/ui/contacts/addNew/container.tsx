import React from "react";
import AddNewPresenter from "./presenter";
import { NEW_URL } from "./../../../data/constants/urls";
import { useHistory } from "react-router-dom";

/** Renders the Add New Contact button. */
const AddNewContainer: React.FunctionComponent = () => {
  // MARK: @config
  /** Router history. */
  const history = useHistory();

  // MARK: @methods
  /** Handles click on the Add New button. */
  const onAddButtonClick = () => {
    history.push(NEW_URL);
  };

  // MARK: @render
  return (
    <AddNewPresenter {...{ onAddButtonClick }} />
  ); };

export default AddNewContainer;
