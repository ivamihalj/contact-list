import { ChangeEvent } from "react";

export interface ContactPresenterProps {
  contacts: Contact[];
  isFavorites: boolean;
  filterContacts(e: ChangeEvent<HTMLInputElement>): void;
}

export interface Contact {
  id: number;
  name: string;
  isFavorite: boolean;
  email?: string;
  image?: string;
  numbers?: ContactNumber[];
}

export interface ContactNumber {
  key: string;
  value: string;
}

export interface ContactNumberInput {
  key: string;
  value: string;
  isKeyValid: boolean;
  isNumberValid: boolean;
}
