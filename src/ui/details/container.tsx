import React from "react";
import { useSelector } from "react-redux";
import { Redirect, RouteComponentProps, useHistory } from "react-router-dom";
import { contactSelectors } from "../../state/selectors";
import DetailsPresenter from "./presenter";
import { HOME_URL } from "./../../data/constants/urls";
import useCreateAction from "../../helpers/hooks/useCreateAction";
import { ContactActions } from "../../state/actions";

/** Renders the contact details page. */
const DetailsContainer: React.FunctionComponent<RouteComponentProps<{id: string}>> = ({
  match,
}) => {
  // MARK: @config
  /** Contact id , from the route. */
  const { id: contactId } = match.params;
  /** Currently open contact. */
  const contact = useSelector((state) => contactSelectors.getContactById(state, Number(contactId)));
  /** Router history. */
  const history = useHistory();

  // MARK: @actions
  /** Handles adding to/removing from favorites. */
  const toggleFavoriteAction = useCreateAction(ContactActions.toggleFavoriteContact);

  // MARK: @methods
  /** Opens the edit form. */
  const editContact = () => {
    history.push(`/edit/${contact?.id}`);
  };

  /** Navigates to the previous URL. */
  const goBack = () => {
    history.goBack();
  };

  /** Toggles a contact favorite. */
  const toggleFavorite = () => {
    toggleFavoriteAction(contact?.id);
  };

  // MARK: @render
  if (!contact) {
    return <Redirect to={HOME_URL} />;
  }

  return (
    <DetailsPresenter {...{ contact, goBack, toggleFavorite, editContact }} />
  ); };

export default DetailsContainer;
