import { Box, Grid } from "@material-ui/core";
import { Edit, Favorite, FavoriteBorder, KeyboardReturn, MailOutline, PhoneOutlined } from "@material-ui/icons";
import React from "react";
import { FormattedMessage } from "react-intl";
import "./styles.scss";
import { DetailsPresenterProps } from "./types";

/** Renders the contact details page.
 * @param {() => void} editContact Opens the edit form.
 * @param {() => void} goBack Navigates to the previous URL.
 * @param {() => } toggleFavorite Toggles a contact favorite.
 * @param {Contact} contact Currently open contact.
*/
const DetailsPresenter: React.FunctionComponent<DetailsPresenterProps> = ({
  editContact,
  goBack,
  toggleFavorite,
  contact,
}) => (
  <Grid
    container
    className="details"
    justify="center"
    spacing={4}
  >
    <Grid item xs={12} md={3}>
      <Grid container xs={12} alignItems="center" className="details__image-container">
        {/* User image */}
        <Grid item xs={3} className="details__image">
          { contact.image ? <img alt="Contact" src={contact.image} /> : <Box /> }
        </Grid>
        {/* User name for mobile */}
        <Grid item xs={9}>
          <Box ml={5} className="details__name">
            {contact.name}
          </Box>
        </Grid>
      </Grid>
    </Grid>
    <Grid item xs={12} md={6} className="details__container">
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        className="details__header"
        pb={2}
        mb={4}
      >
        {/* Contact icons */}
        <Grid container xs={12} className="details__icons">
          {/* Go back icon */}
          <Grid item xs={9} className="details__icons__item">
            <KeyboardReturn
              className="details__button --back"
              onClick={goBack}
            />
          </Grid>
          <Grid item xs={3} className="details__icons__item">
            <Grid container spacing={4} justify="flex-end">
              {/* Toggle favorite icon */}
              <Grid item onClick={toggleFavorite}>
                {contact.isFavorite
                  ? <Favorite fontSize="small" className="details__button" />
                  : <FavoriteBorder fontSize="small" className="details__button" />
                }
              </Grid>
              {/* Edit contact icon */}
              <Grid item>
                <Edit
                  fontSize="small"
                  className="details__button"
                  onClick={editContact}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {/* Contact name */}
        <Box ml={5} className="details__header__name">
          {contact.name}
        </Box>
      </Box>
      {/* Contact e-mail */}
      {contact.email && (
        <Grid container justify="center" className="details__item">
          {/* E-mail label */}
          <Grid item xs={9} md={4} className="details__label">
            <Box mr={2} display="inline-block">
              <MailOutline />
            </Box>
            <Box display="inline-block">
              <FormattedMessage id="contact.email" />
            </Box>
          </Grid>
          {/* E-mail address */}
          <Grid item xs={9} md={6} className="details__info">
            {contact.email}
          </Grid>
        </Grid>
      )}
      {/* Contact numbers */}
      {contact.numbers && (
        <Grid container justify="center" className="details__item">
          {/* Numbers label */}
          <Grid item xs={9} md={4} className="details__label">
            <Box mr={2} display="inline-block">
              <PhoneOutlined />
            </Box>
            <Box display="inline-block">
              <FormattedMessage id="contact.numbers" />
            </Box>
          </Grid>
          {/* Numbers grid */}
          <Grid item xs={9} md={6}>
            {
              contact.numbers?.map((number, index) => (
                <Grid container key={`${number.key}.${index}`} spacing={4} className="details__info">
                  {/* Number label */}
                  <Grid item md={4} className="details__number-label">{number.key}</Grid>
                  {/* Number value */}
                  <Grid item md={8}>{number.value}</Grid>
                </Grid>
              ))
            }
          </Grid>
        </Grid>
      )}
    </Grid>
  </Grid>
);

export default DetailsPresenter;
