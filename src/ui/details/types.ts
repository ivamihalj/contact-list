import { Contact } from "../contacts/types";

export interface DetailsPresenterProps {
  contact: Contact;
  toggleFavorite(): void;
  goBack(): void;
  editContact(): void;
}
