import * as React from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { store } from "../../state/store";
import HeaderContainer from "../header/container";
import Routes from "./Routes";
import messagesEn from "../../data/translations/english.json";
import { IntlProvider } from "react-intl";
import "bootstrap/dist/css/bootstrap.min.css";
import { ThemeProvider } from "@material-ui/core";
import { theme } from "./../../assets/theme";
import "./App.scss";

const messages = {
  en: messagesEn,
};
const language = "en";

const AppContainer: React.FunctionComponent = () => (
  <Provider store={store}>
    <IntlProvider locale={language} messages={messages[language] as Record<string, string>}>
      <BrowserRouter>
        <ThemeProvider {...{ theme }}>
          <HeaderContainer />
          <Routes/>
        </ThemeProvider>
      </BrowserRouter>
    </IntlProvider>
  </Provider>
);

export default AppContainer;
