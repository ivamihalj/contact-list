import React from "react";
import { Route, Switch } from "react-router-dom";
import { FAVORITES_URL, HOME_URL, DETAILS_URL, NEW_URL, EDIT_URL } from "./../../data/constants/urls";
import ContactsContainer from "./../contacts/container";
import DetailsContainer from "./../details/container";
import FormContainer from "./../form/container";


const Routes: React.FunctionComponent<any> = () => (
  <div>
    <Switch>
      <Route path={HOME_URL} exact component={ContactsContainer} />
      <Route path={FAVORITES_URL} exact component={ContactsContainer} />
      <Route path={DETAILS_URL} exact component={DetailsContainer} />
      <Route path={NEW_URL} exact component={FormContainer} />
      <Route path={EDIT_URL} exact component={FormContainer} />
    </Switch>
  </div>
);

export default Routes;
