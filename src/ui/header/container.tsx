import React from "react";
import HeaderPresenter from "./presenter";

/** Renders a header with Typeqast logo. */
const HeaderContainer: React.FunctionComponent = () => (
  // MARK: @render
  <HeaderPresenter />
);

export default HeaderContainer;
