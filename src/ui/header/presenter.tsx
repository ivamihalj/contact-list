import React from "react";
import { typeqastLogo } from "../../assets/images";
import "./styles.scss";

/** Renders the Add/Edit contact form. */
const HeaderPresenter: React.FunctionComponent = () => (
  <div className="header">
    <img className="header__image" src={typeqastLogo} alt="Typeqast logo"/>
  </div>
);

export default HeaderPresenter;
