import { ChangeEvent } from "react";

export interface InputProps {
  onChange?(e: ChangeEvent<HTMLInputElement>): void;
  onFocus?(): any;
  placeholder?: string;
  type?: "number" | "text" | "password" | "email";
  name?: string;
  value?: string;
  isInvalid: boolean;
}
