import React from "react";
import "./styles.scss";
import { InputProps } from "./types";

/** Renders an input element that can be used in multiple components.
 * @param {() => any} onFocus Method executed when focused on this element.
 * @param {boolean} isInvalid If input fails the validation.
 */
const InputPresenter: React.FunctionComponent<InputProps> = ({
  onFocus,
  isInvalid,
  ...props
}) => (
  <input {...props} {...{ onFocus }} className={`input${isInvalid ? " invalid" : ""}`} />
);

export default InputPresenter;
