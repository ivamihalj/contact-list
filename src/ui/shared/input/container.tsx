import React from "react";
import { injectIntl, IntlShape } from "react-intl";
import InputPresenter from "./presenter";
import { InputProps } from "./types";

/** Renders an input element that can be used in multiple components. */
const InputContainer: React.FunctionComponent<InputProps & {intl: IntlShape }> = ({
  intl,
  placeholder,
  type = "text",
  ...props
}) => (
  // MARK: @render
  <InputPresenter
    {...props}
    {...{ type }}
    placeholder={placeholder ? intl.formatMessage({ id: placeholder }) : ""}
  />
);

export default injectIntl(InputContainer);
