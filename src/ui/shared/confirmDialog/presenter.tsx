import { Box, Grid } from "@material-ui/core";
import React from "react";
import { FormattedMessage } from "react-intl";
import ButtonContainer from "../button/container";
import "./styles.scss";
import { ConfirmDialogProps } from "./types";

/** Renders a Confirm/Cancel dialog that can be used in multiple components.
 * @param {() => any} onCancel Method triggered on Cancel button click.
 * @param {() => any} onConfirm Method triggered on Confirm button click.
 * @param {string} cancelLabel Label for the Cancel button.
 * @param {string} confrimLabel Label for the Confirm button.
 * @param {string} label Dialog title.
 * @param {string} message Dialog text.
*/
const ConfirmDialogPresenter: React.FunctionComponent<ConfirmDialogProps> = ({
  onCancel,
  onConfirm,
  cancelLabel = "common.cancel",
  confirmLabel = "common.confirm",
  label,
  message,
}) => (
  <Box className="confirm-dialog" onClick={(e) => e.stopPropagation()}>
    <Box className="confirm-dialog__main">
      {/* Dialog title */}
      <Box className="confirm-dialog__main__title">
        <FormattedMessage id={label} />
      </Box>
      {/* Dialog body */}
      <Box className="confirm-dialog__main__message">
        <FormattedMessage id={message} />
      </Box>
      {/* Dialog buttons */}
      <Grid container className="confirm-dialog__buttons">
        {/* Cancel button */}
        <Grid xs={12} md={6} className="confirm-dialog__button">
          <ButtonContainer variant="secondary" label={cancelLabel} onClick={(onCancel)} />
        </Grid>
        {/* Confirm button */}
        <Grid xs={12} md={6} className="confirm-dialog__button">
          <ButtonContainer variant="primary" label={confirmLabel} onClick={onConfirm} />
        </Grid>
      </Grid>
    </Box>
  </Box>
);

export default ConfirmDialogPresenter;
