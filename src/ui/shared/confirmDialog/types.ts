export interface ConfirmDialogProps {
  label: string;
  message: string;
  isOpen: boolean;
  onConfirm: any;
  onCancel: any;
  confirmLabel?: string;
  cancelLabel?: string;
}
