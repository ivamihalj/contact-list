import React from "react";
import ConfirmDialogPresenter from "./presenter";
import { ConfirmDialogProps } from "./types";

/** Renders a Confirm/Cancel dialog that can be used in multiple components. */
const ConfirmDialogContainer: React.FunctionComponent<ConfirmDialogProps> = ({
  ...props
}) => {
  // MARK: @render
  if (!props.isOpen) { return null; }

  return (
    <ConfirmDialogPresenter {...props} />
  );
};

export default ConfirmDialogContainer;
