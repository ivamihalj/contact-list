import React from "react";
import ButtonPresenter from "./presenter";
import { ButtonProps } from "./types";

/** Renders a button that can be used in multiple components. */
const ButtonContainer: React.FunctionComponent<ButtonProps> = ({
  ...props
}) => (
  // MARK: @render
  <ButtonPresenter {...props} />
);

export default ButtonContainer;
