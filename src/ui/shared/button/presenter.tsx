import React from "react";
import { FormattedMessage } from "react-intl";
import "./styles.scss";
import { ButtonProps } from "./types";

/** Renders a button that can be used in multiple components.
 * @param {() => any} onClick Method triggered on button click.
 * @param {"primary" | "secondary"} variant Primary (confirm) or secondary (cancel).
 * @param {string} label Text to display on the button.
 * @param {"button" | "submit" | "reset"} type Button type.
 */
const ButtonPresenter: React.FunctionComponent<ButtonProps> = ({
  onClick,
  variant,
  label,
  type = "button",
  ...props
}) => (
  <button {...{ type, onClick }} className={`button ${variant}`}>
    <FormattedMessage id={label} />
  </button>
);

export default ButtonPresenter;
