export const HOME_URL = "/";
export const FAVORITES_URL = "/favorites";
export const DETAILS_URL = "/details/:id";
export const EDIT_URL = "/edit/:id";
export const NEW_URL = "/new";
