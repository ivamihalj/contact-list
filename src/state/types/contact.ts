import { Contact } from "../../ui/contacts/types";

export enum ContactActionTypes {
  ToggleFavoriteContact = "TOGGLE_FAVORITE_CONTACT",
  GetAll = "GET_CONTACTS",
  Fetched = "CONTACTS_FETCHED",
  GetOne = "GET_ONE_CONTACT",
  Create = "CREATE_NEW_CONTACT",
  Edit = "EDIT_CONTACT",
  Delete = "DELETE_CONTACT"
}

export interface ContactState {
  contacts: Contact[];
}
