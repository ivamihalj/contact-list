import { Contact } from "../../ui/contacts/types";
import { ContactActionTypes } from "../types/contact";
import { ReduxAction } from "../types/types";

/** Adds contact to or removes contact from favorites.
 * @param {number} contactId Id of a contact to toggle.
 */
const toggleFavoriteContact = (contactId: number): ReduxAction => ({
  type: ContactActionTypes.ToggleFavoriteContact,
  payload: { contactId },
});

/** Deletes a contact from the list.
 * @param {number} contactId Id of a contact to delete.
 */
const deleteContact = (contactId: number): ReduxAction => ({
  type: ContactActionTypes.Delete,
  payload: { contactId },
});

/** Creates a new contact.
 * @param {Contact} contactBody Parameters of the new contact.
 */
const createContact = (contactBody: Contact): ReduxAction => ({
  type: ContactActionTypes.Create,
  payload: { contactBody },
});

/** Edits an existing contact.
 * @param {Contact} contactBody New parameters of the contact.
 */
const editContact = (contactBody: Contact): ReduxAction => ({
  type: ContactActionTypes.Edit,
  payload: { contactBody },
});

export default {
  deleteContact,
  toggleFavoriteContact,
  createContact,
  editContact,
};
