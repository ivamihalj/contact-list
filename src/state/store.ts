import { applyMiddleware, createStore } from "redux";
import { persistStore } from "redux-persist";
import thunk from "redux-thunk";
import rootReducer from "./reducers";

const middlewares = [thunk];

const store = createStore(
  rootReducer,
  {},
  applyMiddleware(...middlewares),
);

const persistor = persistStore(store);

export { store, persistor };
