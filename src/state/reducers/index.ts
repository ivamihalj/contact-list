import { combineReducers, Reducer } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { ReduxAction } from "../types/types";
import contact from "./contact";

const reducers: Reducer = combineReducers({
  contact: persistReducer(
    {
      key: "contact",
      storage,
    },
    contact,
  ),
});

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const rootReducer = (state: any, action: ReduxAction) => reducers(state, action);

export type AppState = ReturnType<typeof rootReducer>;
export default rootReducer;
