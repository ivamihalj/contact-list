import { ReduxAction } from "../types/types";
import { ContactActionTypes, ContactState } from "../types/contact";

const contact = (state: ContactState = { contacts: [] }, action: ReduxAction): ContactState => {
  switch (action.type) {
    /** On contact toggle */
    case ContactActionTypes.ToggleFavoriteContact: {
      const index = state.contacts.findIndex((contact) => contact.id === action.payload.contactId);
      const newContacts = [...state.contacts];
      newContacts[index] = {
        ...newContacts[index],
        isFavorite: !state.contacts[index].isFavorite,
      };
      return {
        ...state,
        contacts: newContacts,
      };
    }
    /** On contact create */
    case ContactActionTypes.Create: {
      const newContact = {
        ...action.payload.contactBody,
        id: state.contacts.length + 1,
        isFavorite: false,
      };
      const newContacts = state.contacts.concat(newContact);
      return {
        ...state,
        contacts: newContacts,
      };
    }
    /** On contact edit */
    case ContactActionTypes.Edit: {
      const index = state.contacts.findIndex((contact) => contact.id === action.payload.contactBody.id);
      const newContacts = [...state.contacts];
      newContacts[index] = {
        ...action.payload.contactBody,
      };
      return {
        ...state,
        contacts: newContacts,
      };
    }
    /** On contact delete */
    case ContactActionTypes.Delete: {
      const newContacts = state.contacts.filter((c) => c.id !== action.payload.contactId);
      return {
        ...state,
        contacts: newContacts,
      };
    }
    default:
      return state;
  }
};

export default contact;
