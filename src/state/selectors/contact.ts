import { Contact } from "../../ui/contacts/types";
import { AppState } from "../reducers";

/** Returns all contacts saved in the store.
 * @param {AppState} state Application state.
 */
const getAllContacts = (state: AppState): Contact[] => state.contact.contacts;

/** Returns favorite contacts from the store.
 * @param {AppState} state Application state.
*/
const getFavoriteContacts = (state: AppState): Contact[] =>
  state.contact.contacts.filter((contact: Contact) => contact.isFavorite);

/** Returns one contact.
  * @param {AppState} state Application state.
  * @param {number} contactId ID of a contact to fetch.
*/
const getContactById = (state: AppState, contactId: number): Contact | undefined =>
  state.contact.contacts.find((c: Contact) => c.id === contactId);

export default {
  getAllContacts,
  getFavoriteContacts,
  getContactById,
};
